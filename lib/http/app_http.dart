import 'package:dio/dio.dart';

class GHttp {
  static const String _getMethod = 'GET';
  static const String _postMethod = 'POST';

  static Future<APIResponse> get(
    String path, {
    data,
    bool needAuthHeaders = true,
    Options? options,
    Map<String, dynamic>? queryParameters,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) {
    return _request(
      path,
      method: _getMethod,
      data: data,
      needAuthHeaders: needAuthHeaders,
      options: options,
      queryParameters: queryParameters,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );
  }

  static Future<APIResponse> post(
    String path, {
    data,
    bool needAuthHeaders = true,
    Options? options,
    Map<String, dynamic>? queryParameters,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) {
    return _request(
      path,
      method: _postMethod,
      data: data,
      needAuthHeaders: needAuthHeaders,
      options: options,
      queryParameters: queryParameters,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );
  }

  static Future<APIResponse> _request(
    String path, {
    required String method,
    data,
    bool needAuthHeaders = true,
    Options? options,
    Map<String, dynamic>? queryParameters,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    APIResponse response;
    final request = await _dio.request(
      path,
      data: data,
      queryParameters: queryParameters,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
      options: (options ?? Options())
        ..method = method
        ..extra = {'needAuthHeaders': needAuthHeaders},
    );

    response = request.data is Map
        ? APIResponse.fromMap(request.data)
        : APIResponse.empty();
    return response;
  }
}

Dio _initDio() {
  var dio = Dio();
  dio.options
    ..connectTimeout = 60000
    ..receiveTimeout = 60000;

  return dio;
}

Dio _dio = _initDio();

class APIResponse {
  final String code;
  final String message;
  final dynamic data;
  final int httpCode;

  APIResponse({
    required this.code,
    required this.message,
    required this.data,
    required this.httpCode,
  });

  factory APIResponse.fromMap(Map<String, dynamic> map) {
    return APIResponse(
      code: map['code'],
      message: map['message'],
      data: map['data'],
      httpCode: map['httpCode'],
    );
  }

  factory APIResponse.empty() {
    return APIResponse(
      code: '',
      message: '',
      data: '',
      httpCode: -1,
    );
  }
}
