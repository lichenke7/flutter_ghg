import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:ghg/utils/color_values.dart';
import 'package:oktoast/oktoast.dart';

import 'app/modules/splash/controllers/splash_service.dart';
import 'app/routes/app_pages.dart';
import 'app/services/auth_service.dart';

void main() {
  runApp(const MyApp());

  if (Platform.isAndroid) {
    // 以下两行 设置android状态栏为透明的沉浸。写在组件渲染之后，是为了在渲染后进行set赋值，覆盖状态栏，写在渲染之前MaterialApp组件会覆盖掉这个值。
    SystemUiOverlayStyle systemUiOverlayStyle =
        const SystemUiOverlayStyle(statusBarColor: GColors.transparent);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 690),
      minTextAdapt: true,
      builder: () => OKToast(
        child: GetMaterialApp(
          debugShowCheckedModeBanner: false,
          enableLog: kDebugMode,
          theme: ThemeData(
            brightness: Brightness.light,
            primarySwatch: GColors.primarySwatch,
          ),
          initialBinding: BindingsBuilder(
            () {
              Get.put(SplashService());
              Get.put(AuthService());
            },
          ),
          getPages: AppPages.routes,
          initialRoute: AppPages.initial,
          // locale: TranslationService.locale,
          // fallbackLocale: TranslationService.fallbackLocale,
          // home: LoginView(),
          // builder: (context, child) {
          //   // ScreenUtil.setContext(context);
          //   return MediaQuery(
          //     //Setting font does not change with system font size
          //     data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
          //     child: FutureBuilder<void>(
          //       key: const ValueKey('initFuture'),
          //       future: Get.find<SplashService>().init(),
          //       builder: (context, snapshot) {
          //         if (snapshot.connectionState == ConnectionState.done) {
          //           return child ?? const SizedBox.shrink();
          //         }
          //         return SplashView();
          //       },
          //     ),
          //   );
          // },
        ),
      ),
    );
  }
}
