import 'package:flutter/material.dart';

extension FontWeightEx on FontWeight {
  static const FontWeight bold = FontWeight.w600;
  static const FontWeight normal = FontWeight.normal;
}
