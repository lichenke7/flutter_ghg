import 'package:flutter_screenutil/flutter_screenutil.dart';

class GSize {
  GSize._();

  static double commonTextSize = 14.sm;
  static double commonTitleSize = 18.sm;
}
