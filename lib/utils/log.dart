import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

void gLog(String message) {
  if (kDebugMode) {
    Get.log('$message');
  }
}
