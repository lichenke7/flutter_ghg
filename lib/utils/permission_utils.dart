import 'package:ghg/app/widgets/common_dialog.dart';
import 'package:permission_handler/permission_handler.dart';

/// 申请权限
Future<bool?> requestPermission(
  Permission permission, {
  required String title,
  required String content,
  String? confirm,
  String? cancel,
}) async {
  final status = await permission.status;
  return status.isDenied || status.isPermanentlyDenied
      ? await _requestPermissionResult(
          permission,
          title: title,
          content: content,
          confirm: confirm,
          cancel: cancel,
        )
      : true;
}

Future<bool?> _requestPermissionResult(
  Permission permission, {
  required String title,
  required String content,
  String? confirm,
  String? cancel,
}) async {
  final dialogResult = await showGHGDialog(
    title: _permissionTitle(permission),
    content: _permissionDesc(permission),
  );

  if (dialogResult == true) {
    final result = await permission.request();
    if (result.isDenied || result.isPermanentlyDenied) {
      openAppSettings();
    } else if (result.isGranted) {
      return true;
    }
  }

  return false;
}

/// 权限申请标题
String _permissionTitle(Permission permission) {
  String permissionType = '';
  if (permission == Permission.camera) {
    permissionType = '相机';
  }

  return '申请$permissionType权限';
}

/// 权限申请说明
String _permissionDesc(Permission permission) {
  String permissionDesc = '';
  if (permission == Permission.camera) {
    permissionDesc = '更换头像需要使用相机权限';
  }

  return permissionDesc;
}
