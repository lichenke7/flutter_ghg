import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:ghg/utils/color_values.dart';
import 'package:ghg/utils/fontweight_ex.dart';

TextStyle get buttonTextStyle => TextStyle(
      color: GColors.white,
      fontSize: 15.sp,
      fontWeight: FontWeightEx.bold,
    );

class GButton extends StatelessWidget {
  final Color? buttonColor;
  final Size? buttonSize;
  final TextStyle? textStyle;
  final String text;
  final VoidCallback? onTap;
  final Color? disabledColor;
  const GButton({
    Key? key,
    this.buttonColor,
    this.buttonSize,
    required this.text,
    this.onTap,
    this.textStyle,
    this.disabledColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      color: buttonColor,
      disabledColor: disabledColor ?? GColors.black12,
      child: Container(
        width: buttonSize?.width,
        height: buttonSize?.height ?? 45.sp,
        alignment: Alignment.center,
        child: Text(
          text,
          style: textStyle ?? buttonTextStyle,
        ),
      ),
      onPressed: onTap,
    );
  }
}
