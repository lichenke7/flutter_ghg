import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:ghg/utils/color_values.dart';

class ListWidget extends StatefulWidget {
  final List<ListBean> data;
  const ListWidget({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  _ListWidgetState createState() => _ListWidgetState();
}

class _ListWidgetState extends State<ListWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: GColors.backgroundColor,
      child: ListView.separated(
        itemBuilder: (_, index) {
          final item = widget.data[index];
          return ListTile(
            focusColor: Colors.pink,
            enabled: true,
            hoverColor: Colors.pinkAccent,
            trailing: const Icon(Icons.keyboard_arrow_right),
            selected: true,
            title: Text(
              item.name,
              style: TextStyle(fontSize: 14.sp),
            ),
            onTap: item.onTap,
          );
        },
        separatorBuilder: (_, index) {
          return SizedBox(
            height: 10.sp,
          );
        },
        itemCount: widget.data.length,
      ),
    );
  }
}

class ListBean {
  final String name;
  final VoidCallback onTap;

  ListBean(this.name, {required this.onTap});
}
