import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ghg/utils/color_values.dart';
import 'package:ghg/utils/size_values.dart';

// 中间阴影背景的弹窗
// Get.defaultDialog(
//   title: 'Test Dialog In Home Outlet !!',
//   barrierDismissible: true,
//   navigatorKey: Get.nestedKey(Routes.HOME),
// );
// 全局阴影背景的弹窗
// Get.defaultDialog(
//   title: 'Test Dialog !!',
//   barrierDismissible: true,
// );

/// 通用弹窗（标题、内容、取消、确认）
Future<bool?> showGHGDialog({
  required String title,
  required String content,
  String? confirm,
  String? cancel,
}) {
  return Get.defaultDialog(
    title: title,
    middleText: content,
    textConfirm: confirm ?? '确认',
    textCancel: cancel ?? '取消',
    onConfirm: () => Get.back(result: true),
    barrierDismissible: true,
    onWillPop: () => Future.value(false),
  );
}

TextStyle get _buildCommonTextWidget => TextStyle(
      color: GColors.dialogSubTitleColor,
      fontSize: GSize.commonTextSize,
      fontWeight: FontWeight.normal,
    );
