import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ghg/utils/color_values.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  final double contentHeight;
  final Widget? rightChild;
  final String title;
  const CustomAppBar({
    Key? key,
    this.contentHeight = 44,
    this.title = '',
    this.rightChild,
  }) : super(key: key);

  @override
  _CustomAppBarState createState() => _CustomAppBarState();

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(contentHeight);
}

class _CustomAppBarState extends State<CustomAppBar> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          IconButton(
            onPressed: Get.back,
            icon: Icon(
              Icons.arrow_back_ios_rounded,
              color: GColors.grey,
            ),
          ),
          Spacer(),
          if (widget.rightChild != null) widget.rightChild!,
        ],
      ),
    );
  }
}
