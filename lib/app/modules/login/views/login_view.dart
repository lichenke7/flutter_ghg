import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:ghg/app/widgets/common_widgets.dart';
import 'package:ghg/utils/color_values.dart';
import 'package:ghg/utils/fontweight_ex.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  LoginView({Key? key}) : super(key: key);
  late BuildContext context;
  final TextEditingController _unameController = TextEditingController();
  final TextEditingController _pwdController = TextEditingController();

  List<Widget> get _loginWidgetList => _buildWelcomeWidget();

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Scaffold(
        extendBody: true,
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 20.sp),
          child: ListView.builder(
            itemBuilder: (context, index) {
              return _loginWidgetList[index];
            },
            itemCount: _loginWidgetList.length,
          ),
        ),
      ),
    );
  }

  List<Widget> _buildWelcomeWidget() {
    return [
      SizedBox(height: Get.statusBarHeight),
      Text(
        '欢迎登录 Seeker',
        textAlign: TextAlign.center,
        style: TextStyle(
          color: GColors.primarySwatch,
          fontSize: 24.sp,
          fontWeight: FontWeightEx.bold,
        ),
      ),
      SizedBox(
        height: 30.sm,
      ),
      TextField(
        autofocus: true,
        controller: _unameController,
        decoration: const InputDecoration(
            labelText: "用户名",
            hintText: "用户名或邮箱",
            prefixIcon: Icon(Icons.person)),
      ),
      TextField(
        controller: _pwdController,
        decoration: const InputDecoration(
            labelText: "密码", hintText: "您的登录密码", prefixIcon: Icon(Icons.lock)),
        obscureText: true,
      ),
      SizedBox(
        height: 15.sp,
      ),
      Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Obx(
            () => Checkbox(
              onChanged: controller.selected,
              value: controller.selected.isTrue,
            ),
          ),
          RichText(
            text: TextSpan(
              text: '我已详读并同意',
              style: TextStyle(
                fontSize: 13.sp,
                color: GColors.black,
                fontWeight: FontWeight.normal,
              ),
              children: [
                TextSpan(
                  text: '<用户协议>',
                  style: TextStyle(color: GColors.primarySwatch),
                ),
                TextSpan(text: '与'),
                TextSpan(
                  text: '<隐私协议>',
                  style: TextStyle(color: GColors.primarySwatch),
                ),
              ],
            ),
          ),
        ],
      ),
      SizedBox(
        height: 25.sm,
      ),
      GButton(
        text: '登录',
        buttonColor: GColors.primarySwatch,
        onTap: () => controller.login(
          context,
          name: _unameController.text,
          pwd: _pwdController.text,
        ),
      ),
      SizedBox(height: 10.sm),
      GButton(
        text: '注册',
        onTap: controller.register,
        textStyle: buttonTextStyle.copyWith(color: GColors.primarySwatch),
      ),
    ];
  }
}
