import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:ghg/app/modules/login/controllers/register_controller.dart';
import 'package:ghg/app/routes/app_pages.dart';
import 'package:ghg/app/widgets/common_widgets.dart';
import 'package:ghg/app/widgets/custom_appbar.dart';
import 'package:ghg/utils/color_values.dart';
import 'package:ghg/utils/fontweight_ex.dart';
import 'package:ghg/utils/num_value.dart';

class RegisterView extends GetView<RegisterController> {
  final TextEditingController _codeController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _pwdController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Scaffold(
        extendBody: true,
        appBar: CustomAppBar(
          contentHeight: 50.sm,
          rightChild: GButton(
            text: '登录',
            onTap: () => Get.offNamed(Routes.LOGIN),
            textStyle: buttonTextStyle.copyWith(color: GColors.primarySwatch),
          ),
        ),
        body: Obx(() => _buildChildWidget()),
      ),
    );
  }

  String get timerText => controller.codeInterval.value > NumConstant.int_60
      ? '获取验证码'
      : '重新获取${controller.codeInterval}';
  bool get ableSendInviteCode =>
      controller.email.isNotEmpty &&
      controller.codeInterval > NumConstant.int_60;
  bool get ableRegister =>
      controller.tapGetCode.isTrue &&
      controller.email.isNotEmpty &&
      controller.code.isNotEmpty;
  List<Widget> get _widgetList => _buildRegisterWidgetList();

  Widget _buildChildWidget() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.sp),
      child: ListView.builder(
        itemBuilder: (context, index) {
          return _widgetList[index];
        },
        itemCount: _widgetList.length,
      ),
    );
  }

  List<Widget> _buildRegisterWidgetList() {
    return [
      SizedBox(height: 20.sm),
      Text(
        '欢迎注册 Seeker\n',
        textAlign: TextAlign.center,
        style: TextStyle(
          color: GColors.primarySwatch,
          fontSize: 24.sp,
          fontWeight: FontWeightEx.bold,
        ),
      ),
      SizedBox(height: 30.sm),
      TextField(
        autofocus: true,
        controller: _emailController,
        onChanged: (value) {
          controller.email(value);
        },
        decoration: const InputDecoration(
            labelText: "邮箱",
            hintText: "请输入邮箱",
            prefixIcon: Icon(Icons.email_outlined)),
      ),
      Row(
        children: [
          Expanded(
            child: TextField(
              autofocus: true,
              controller: _codeController,
              onChanged: (value) {
                controller.code(value);
              },
              decoration: const InputDecoration(
                  labelText: "验证码",
                  hintText: "请输入邮箱验证码",
                  prefixIcon: Icon(Icons.code_rounded)),
            ),
          ),
          GButton(
            text: timerText,
            onTap: ableSendInviteCode ? controller.sendInviteCode : null,
            buttonColor: GColors.primarySwatch,
          ),
        ],
      ),
      TextField(
        controller: _pwdController,
        decoration: const InputDecoration(
          labelText: "密码",
          hintText: "请设置密码",
          prefixIcon: Icon(Icons.lock),
        ),
        obscureText: true,
      ),
      TextField(
        controller: _pwdController,
        decoration: const InputDecoration(
          labelText: "密码确认",
          hintText: "二次密码确认",
          prefixIcon: Icon(Icons.lock),
        ),
        obscureText: true,
      ),
      SizedBox(height: 15.sm),
      Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Checkbox(
            onChanged: controller.selected,
            value: controller.selected.isTrue,
          ),
          RichText(
            text: TextSpan(
              text: '我已详读并同意',
              style: TextStyle(
                fontSize: 13.sp,
                color: GColors.black,
                fontWeight: FontWeight.normal,
              ),
              children: [
                TextSpan(
                  text: '<用户协议>',
                  style: TextStyle(color: GColors.primarySwatch),
                ),
                TextSpan(text: '与'),
                TextSpan(
                  text: '<隐私协议>',
                  style: TextStyle(color: GColors.primarySwatch),
                ),
              ],
            ),
          ),
        ],
      ),
      SizedBox(height: 25.sm),
      GButton(
        text: '注册',
        buttonColor: GColors.primarySwatch,
        onTap: ableRegister ? controller.register : null,
      ),
    ];
  }
}
