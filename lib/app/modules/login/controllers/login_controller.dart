import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:ghg/app/modules/login/data/login_repository.dart';
import 'package:ghg/app/modules/login/models/login.dart';
import 'package:ghg/app/routes/app_pages.dart';
import 'package:ghg/app/services/auth_service.dart';
import 'package:ghg/utils/log.dart';
import 'package:oktoast/oktoast.dart';

class LoginController extends GetxController {
  RxBool selected = false.obs;
  LoginController({required this.repository});

  final ILoginRepository repository;

  Future<void> login(
    BuildContext context, {
    required String name,
    required String pwd,
  }) async {
    if (name.isBlank == true || pwd.isBlank == true) {
      showToast('用户名或密码不能为空');
      return;
    }

    if (selected.isFalse) {
      showToast('请阅读并同意用户协议和隐私协议');
      return;
    }
    final response =
        await repository.postLogin(LoginModel(email: name, password: pwd));

    if (response) {
      AuthService.to.login(
        name: name,
        pwd: pwd,
      );
      final thenTo = context.params['then'];
      Get.offNamed(thenTo ?? Routes.HOME);
    } else {
      showToast('登录失败，请稍后重试');
    }
  }

  void register() {
    gLog('register');
    Get.toNamed(Routes.INVITE_CODE);
  }

  void startRegister() {}
}
