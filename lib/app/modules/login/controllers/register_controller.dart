import 'dart:async';

import 'package:get/get.dart';
import 'package:ghg/utils/log.dart';
import 'package:ghg/utils/num_value.dart';
import 'package:oktoast/oktoast.dart';

class RegisterController extends GetxController {
  RxInt codeInterval = (NumConstant.int_60 + 1).obs;
  Timer? _timer;
  RxString email = ''.obs;
  RxString code = ''.obs;
  RxBool tapGetCode = false.obs;
  RxBool selected = false.obs;

  @override
  void onInit() {
    super.onInit();
    gLog('RegisterController---onInit');
  }

  @override
  void onClose() {
    _cancelTimer();
    super.onClose();
  }

  void startRegister() {}

  void sendInviteCode() {
    tapGetCode(true);
    codeInterval(NumConstant.int_60);
    _timer = Timer.periodic(Duration(seconds: 1), (_timer) {
      gLog('codeInterval: $codeInterval');
      codeInterval--;
      if (codeInterval.value == 0) {
        codeInterval(NumConstant.int_60 + 1);
        _timer.cancel();
      }
    });
  }

  void register() {
    if (selected.isFalse) {
      showToast('请阅读并同意用户协议和隐私协议');
      return;
    }
  }

  void _cancelTimer() {
    _timer?.cancel();
    _timer == null;
  }
}
