import 'package:get/get.dart';
import 'package:ghg/app/base/base_binding.dart';
import 'package:ghg/app/modules/login/controllers/register_controller.dart';

class RegisterBinding extends BaseBinding {
  @override
  List<Bind> dependencies() {
    return [
      Bind.lazyPut<RegisterController>(() => RegisterController()),
    ];
  }
}
