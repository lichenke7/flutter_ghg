import 'package:get/get.dart';
import 'package:ghg/app/base/base_binding.dart';
import 'package:ghg/app/modules/login/data/login_api_provider.dart';
import 'package:ghg/app/modules/login/data/login_repository.dart';

import '../controllers/login_controller.dart';

class LoginBinding extends BaseBinding {
  @override
  List<Bind> dependencies() {
    return [
      Bind.lazyPut<ILoginProvider>(() => LoginProvider()),
      Bind.lazyPut<ILoginRepository>(
          () => LoginRepository(provider: Get.find())),
      Bind.lazyPut<LoginController>(
          () => LoginController(repository: Get.find())),
    ];
  }
}
