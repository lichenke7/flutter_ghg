import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:ghg/app/modules/login/invite/controllers/invite_code_controller.dart';
import 'package:ghg/app/widgets/common_widgets.dart';
import 'package:ghg/app/widgets/custom_appbar.dart';
import 'package:ghg/utils/color_values.dart';
import 'package:ghg/utils/fontweight_ex.dart';

class InviteCodeView extends GetView<InviteCodeController> {
  final TextEditingController _inviteCodeController = TextEditingController();

  InviteCodeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Scaffold(
        extendBody: true,
        appBar: CustomAppBar(
          contentHeight: 50.sm,
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 20.sp),
          child: Column(
            children: [
              SizedBox(height: 20.sm),
              Text(
                '欢迎注册 Seeker',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: GColors.primarySwatch,
                  fontSize: 24.sp,
                  fontWeight: FontWeightEx.bold,
                ),
              ),
              SizedBox(height: 5.sm),
              Text(
                '请填写邀请码',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: GColors.primarySwatch,
                  fontSize: 16.sp,
                  fontWeight: FontWeightEx.bold,
                ),
              ),
              SizedBox(height: 30.sm),
              TextField(
                autofocus: true,
                controller: _inviteCodeController,
                decoration: const InputDecoration(
                  labelText: "邀请码",
                  hintText: "请输入邀请码或链接",
                  prefixIcon: Icon(Icons.code_rounded),
                ),
              ),
              SizedBox(height: 30.sm),
              GButton(
                text: '开启注册',
                buttonColor: GColors.primarySwatch,
                onTap: () =>
                    controller.startRegister(_inviteCodeController.text),
              ),
              SizedBox(height: 10.sm),
              GButton(
                text: '预约开放',
                onTap: controller.appointmentRegister,
                textStyle:
                    buttonTextStyle.copyWith(color: GColors.primarySwatch),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
