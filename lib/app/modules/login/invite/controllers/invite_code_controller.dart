import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:ghg/app/routes/app_pages.dart';
import 'package:ghg/app/widgets/common_widgets.dart';
import 'package:ghg/utils/color_values.dart';
import 'package:ghg/utils/log.dart';
import 'package:oktoast/oktoast.dart';

class InviteCodeController extends GetxController {
  @override
  void onInit() {
    super.onInit();
    gLog('RegisterController---onInit');
  }

  void startRegister(String? inviteMsg) {
    if (inviteMsg == null || inviteMsg.isEmpty) {
      showToast('邀请码不能为空');
      return;
    }
    Get.toNamed(Routes.REGISTER, parameters: {'inviteMsg': inviteMsg});
  }

  Future<void> appointmentRegister() async {
    // show a dialog
    final TextEditingController _unameController = TextEditingController();
    final childWidgets = [
      SizedBox(height: 10.sm),
      Text(
        '请填写邮箱预约 Seeker',
        style: buttonTextStyle.copyWith(
          color: GColors.primarySwatch,
          fontSize: 20.sp,
        ),
        textAlign: TextAlign.center,
      ),
      SizedBox(height: 20.sm),
      TextField(
        autofocus: true,
        controller: _unameController,
        decoration: const InputDecoration(
            labelText: "邮箱",
            hintText: "请输入预约邮箱",
            prefixIcon: Icon(Icons.email)),
      ),
      SizedBox(height: 10.sm),
      Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(width: 30.sm),
          GButton(
            text: '放弃',
            onTap: () => Get.back(result: ''),
            textStyle: buttonTextStyle.copyWith(color: GColors.primarySwatch),
          ),
          const Spacer(),
          GButton(
            text: '预约',
            buttonColor: GColors.primarySwatch,
            onTap: () => Get.back(result: _unameController.text),
          ),
          SizedBox(width: 30.sm),
        ],
      ),
    ];
    final widget = Material(
      color: GColors.transparent,
      child: Align(
        alignment: Alignment.center,
        child: Container(
          width: double.infinity,
          constraints: BoxConstraints(minHeight: 0, maxHeight: 200.sm),
          padding: EdgeInsets.all(15.sm),
          margin: EdgeInsets.all(20.sm),
          decoration: BoxDecoration(
            color: GColors.white,
            borderRadius: BorderRadius.circular(15.sm),
          ),
          child: ListView.builder(
            itemBuilder: (context, index) {
              return childWidgets[index];
            },
            itemCount: childWidgets.length,
          ),
        ),
      ),
    );
    final result = await Get.dialog(widget);
    gLog('result: $result');
  }
}
