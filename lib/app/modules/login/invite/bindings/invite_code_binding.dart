import 'package:get/get.dart';
import 'package:ghg/app/base/base_binding.dart';
import 'package:ghg/app/modules/login/invite/controllers/invite_code_controller.dart';

class InviteCodeBinding extends BaseBinding {
  @override
  List<Bind> dependencies() {
    return [
      Bind.lazyPut<InviteCodeController>(() => InviteCodeController()),
    ];
  }
}
