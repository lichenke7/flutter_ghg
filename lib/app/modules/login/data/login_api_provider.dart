import 'package:get/get.dart';
import 'package:ghg/app/base/base_provider.dart';
import 'package:ghg/app/models/base.dart';
import 'package:ghg/app/modules/login/models/login.dart';

abstract class ILoginProvider {
  Future<Response<BaseHttpModel>> postLogin(LoginModel model);
}

class LoginProvider extends BaseProvider implements ILoginProvider {
  @override
  Future<Response<BaseHttpModel>> postLogin(LoginModel model) {
    return post('$baseUrl/login', {
      'email': model.email,
      'password': model.password,
    });
  }
}
