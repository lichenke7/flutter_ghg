import 'package:ghg/app/modules/login/models/login.dart';

import 'login_api_provider.dart';

abstract class ILoginRepository {
  Future<bool> postLogin(LoginModel model);
}

class LoginRepository implements ILoginRepository {
  LoginRepository({required this.provider});
  final ILoginProvider provider;

  @override
  Future<bool> postLogin(LoginModel model) async {
    final cases = await provider.postLogin(model);
    if (cases.status.hasError) {
      return Future.error(cases.statusText!);
    } else {
      return cases.body?.success ?? false;
    }
  }
}
