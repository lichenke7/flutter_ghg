import 'dart:convert';

class LoginModel {
  final String email;
  final String password;
  LoginModel({
    required this.email,
    required this.password,
  });

  factory LoginModel.fromRawJson(String str) =>
      LoginModel.fromJson(json.decode(str) as Map<String, dynamic>);

  String toRawJson() => json.encode(toJson());

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
        email: json["email"] as String,
        password: json["password"] as String,
      );

  Map<String, dynamic> toJson() => {
        "email": email,
        "password": password,
      };
}
