import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ghg/app/modules/dashboard/views/dashboard_view.dart';
import 'package:ghg/app/modules/profile/views/profile_view.dart';
import 'package:ghg/app/modules/score_list/views/score_list_view.dart';
import 'package:ghg/app/routes/app_pages.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);

  List get pages => [
        DashboardView(),
        ScoreListView(),
        ProfileView(),
      ];

  @override
  Widget build(BuildContext context) {
    return GetRouterOutlet.builder(
      routerDelegate: Get.nestedKey(Routes.HOME),
      builder: (context) {
        final delegate = context.navigation;
        //This router outlet handles the appbar and the bottom navigation bar
        final currentLocation = context.location;
        var currentIndex = -1;
        if (currentLocation.endsWith('/') == true ||
            currentLocation.endsWith(Routes.HOME) == true) {
          currentIndex = 0;
        }

        if (currentLocation.endsWith(Routes.SCORE_LIST) == true) {
          currentIndex = 1;
        }
        if (currentLocation.endsWith(Routes.PROFILE) == true) {
          currentIndex = 2;
        }

        return Scaffold(
          body: GetRouterOutlet(
            initialRoute: Routes.DASHBOARD,
            anchorRoute: Routes.HOME,
          ),
          bottomNavigationBar: currentIndex < 0
              ? null
              : CupertinoTabBar(
                  currentIndex: currentIndex,
                  onTap: (value) {
                    switch (value) {
                      case 0:
                        delegate.toNamed(Routes.HOME);
                        break;
                      case 2:
                        delegate.toNamed(Routes.PROFILE);
                        break;
                      case 1:
                        delegate.toNamed(Routes.SCORE_LIST);
                        break;
                      default:
                    }
                  },
                  items: const [
                    // _Paths.HOME + [Empty]
                    BottomNavigationBarItem(
                      icon: Icon(Icons.home_outlined),
                      label: '主页',
                    ),
                    // _Paths.HOME + _Paths.PRODUCTS
                    BottomNavigationBarItem(
                      icon: Icon(Icons.star_outline),
                      label: '打分',
                    ),
                    // _Paths.HOME + Routes.PROFILE
                    BottomNavigationBarItem(
                      icon: Icon(Icons.account_circle_outlined),
                      label: '我的',
                    ),
                  ],
                ),
        );
      },
    );
  }
}
