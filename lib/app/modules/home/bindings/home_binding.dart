import 'package:get/get.dart';
import 'package:ghg/app/base/base_binding.dart';

import '../controllers/home_controller.dart';

class HomeBinding extends BaseBinding {
  @override
  List<Bind> dependencies() {
    return [
      Bind.lazyPut<HomeController>(() => HomeController()),
    ];
  }
}
