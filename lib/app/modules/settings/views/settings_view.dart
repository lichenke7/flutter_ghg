import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ghg/app/widgets/list_widget.dart';

import '../controllers/settings_controller.dart';

class SettingsView extends GetView<SettingsController> {
  SettingsView({Key? key}) : super(key: key);
  // final controller = Get.put<SettingsController>(SettingsController());

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('SettingsView'),
        ),
        body: ListWidget(
          data: controller.dataList,
        ),
      ),
      onWillPop: () {
        return Future.value(false);
      },
    );
  }
}
