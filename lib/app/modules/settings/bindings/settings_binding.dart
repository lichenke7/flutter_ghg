import 'package:get/get.dart';
import 'package:ghg/app/base/base_binding.dart';

import '../controllers/settings_controller.dart';

class SettingsBinding extends BaseBinding {
  @override
  List<Bind> dependencies() {
    return [
      Bind.lazyPut<SettingsController>(() => SettingsController()),
    ];
  }
}
