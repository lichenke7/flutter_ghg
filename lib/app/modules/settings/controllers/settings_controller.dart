import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ghg/app/modules/debug/views/debug_tools_page.dart';
import 'package:ghg/app/services/auth_service.dart';
import 'package:ghg/app/widgets/common_dialog.dart';
import 'package:ghg/app/widgets/list_widget.dart';
import 'package:ghg/utils/permission_utils.dart';
import 'package:oktoast/oktoast.dart';
import 'package:permission_handler/permission_handler.dart';

class SettingsController extends GetxController {
  final List<ListBean> dataList = [];
  late final List<ListBean> toolsDataList;

  @override
  void onInit() {
    super.onInit();
    dataList.add(
      ListBean(
        'DebugTools',
        onTap: () => navigateToDebugToolsPage(Get.context!),
      ),
    );
    dataList.add(
      ListBean(
        '退出登录',
        onTap: () => AuthService.to.logout(),
      ),
    );
    dataList.add(
      ListBean(
        'back',
        onTap: () {
          Get.back();
        },
      ),
    );
    toolsDataList = [
      ListBean('show dialog', onTap: () => _onListItemTap(0)),
      ListBean('request permission', onTap: () => _onListItemTap(1))
    ];
  }

  Future<void> _onListItemTap(int index) async {
    switch (index) {
      case 0:
        final result = await showGHGDialog(
            title: '弹窗标题', content: '我是弹窗内容，包含内容1内容2内容3...');
        debugPrint('result: $result');
        break;
      case 1:
        final requestResult = await requestPermission(Permission.camera,
            title: '申请相机权限', content: 'ghg在更换头像场景申请使用相机权限');
        showToast(
          '权限${requestResult == true ? '成功' : '失败'}',
          context: Get.context!,
          position: ToastPosition.bottom,
          backgroundColor: Colors.black.withOpacity(0.8),
          radius: 13.0,
          textStyle: const TextStyle(fontSize: 18.0),
          animationBuilder: const Miui10AnimBuilder(),
        );
        break;
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
