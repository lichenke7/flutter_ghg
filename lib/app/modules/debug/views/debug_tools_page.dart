import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ghg/app/modules/settings/controllers/settings_controller.dart';
import 'package:ghg/app/routes/app_pages.dart';
import 'package:ghg/app/widgets/list_widget.dart';

void navigateToDebugToolsPage(BuildContext context) {
  Navigator.of(context).push(
    CupertinoPageRoute(
      builder: (BuildContext context) {
        return const DebugToolsPage();
      },
      settings: const RouteSettings(name: Routes.DEBUG_TOOLS),
    ),
  );
}

class DebugToolsPage extends GetView<SettingsController> {
  const DebugToolsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('DebugToolsPage'),
      ),
      body: ListWidget(
        data: controller.toolsDataList,
      ),
    );
  }
}
