import 'package:get/get.dart';
import 'package:ghg/app/base/base_binding.dart';

import '../controllers/root_controller.dart';

class RootBinding extends BaseBinding {
  @override
  List<Bind> dependencies() {
    return [
      Bind.lazyPut<RootController>(() => RootController()),
    ];
  }
}
