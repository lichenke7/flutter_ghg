import 'package:get/get.dart';
import 'package:ghg/app/base/base_binding.dart';

import '../controllers/profile_controller.dart';

class ProfileBinding extends BaseBinding {
  @override
  List<Bind> dependencies() {
    return [
      Bind.lazyPut<ProfileController>(() => ProfileController()),
    ];
  }
}
