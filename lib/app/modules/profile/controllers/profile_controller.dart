import 'package:get/get.dart';
import 'package:ghg/app/base/base_controller.dart';
import 'package:ghg/app/routes/app_pages.dart';

class ProfileController extends BaseGetXController {
  void gotoSettings() {
    // Navigator.of(Get.context!).push(
    //   CupertinoPageRoute(
    //     builder: (BuildContext context) {
    //       return SettingsView();
    //     },
    //     // settings: const RouteSettings(name: Routes.DEBUG_TOOLS),
    //   ),
    // );

    Get.toNamed(Routes.SETTINGS);
  }
}
