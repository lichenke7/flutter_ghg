import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/profile_controller.dart';

class ProfileView extends StatelessWidget {
  final ProfileController controller = Get.put(ProfileController());

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(
          appBar: AppBar(
            title: Text('ProfileView'),
            actions: [
              IconButton(
                onPressed: controller.gotoSettings,
                icon: const Icon(Icons.settings),
              ),
            ],
          ),
          body: Container(
            alignment: Alignment.topCenter,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: 50,
                ),
                Hero(
                  tag: 'heroLogo',
                  child: FlutterLogo(size: 50),
                ),
                SizedBox(
                  height: 20,
                ),
                const Text(
                  '离歌笑',
                  style: TextStyle(fontSize: 20),
                ),
                SizedBox(
                  height: 10,
                ),
                const Text(
                  'xxxxx签名信息',
                  style: TextStyle(fontSize: 15),
                ),
              ],
            ),
          ),
        ),
        onWillPop: () async {
          return false;
        });
  }
}
