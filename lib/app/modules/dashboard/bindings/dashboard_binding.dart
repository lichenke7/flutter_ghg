import 'package:get/get.dart';
import 'package:ghg/app/base/base_binding.dart';

import '../controllers/dashboard_controller.dart';

class DashboardBinding extends BaseBinding {
  @override
  List<Bind> dependencies() {
    return [
      Bind.lazyPut<DashboardController>(() => DashboardController()),
    ];
  }
}
