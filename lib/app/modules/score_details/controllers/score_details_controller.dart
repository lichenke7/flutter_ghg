import 'package:get/get.dart';

class ScoreDetailsController extends GetxController {
  late String productId;

  ScoreDetailsController();
  @override
  void onInit() {
    super.onInit();
    productId = Get.parameters['scoreId'] ?? '';
    Get.log('ScoreDetailsController created with id: $productId');
  }

  @override
  void onClose() {
    Get.log('ScoreDetailsController close with id: $productId');
    super.onClose();
  }

  void back() {
    // Navigator.pop(Get.context!);
    Get.back();
    // onClose();
    // onDelete();
  }
}
