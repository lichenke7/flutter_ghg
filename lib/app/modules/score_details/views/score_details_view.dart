import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ghg/app/modules/score_list/controllers/score_list_controller.dart';

class ScoreDetailsView extends GetView<ScoreListController> {
  ScoreDetailsView({Key? key}) : super(key: key);
  // final ScoreDetailsController controller = Get.put(ScoreDetailsController());

  @override
  Widget build(BuildContext context) {
    // WillPopScope
    return Scaffold(
      appBar: AppBar(
        title: Text('ScoreDetailsView'),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Text(
              'ScoreDetailsView is working',
              style: TextStyle(fontSize: 20),
            ),
            Text('ScoreId: ${controller.productId}')
          ],
        ),
      ),
    );
  }
}
