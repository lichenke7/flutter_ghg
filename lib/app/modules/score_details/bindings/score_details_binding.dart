import 'package:get/get.dart';
import 'package:ghg/app/base/base_binding.dart';

import '../controllers/score_details_controller.dart';

class ScoreDetailsBinding extends BaseBinding {
  @override
  List<Bind> dependencies() {
    return [
      Bind.create(() => ScoreDetailsController()),
    ];
  }
}
