import 'package:get/get.dart';
import 'package:ghg/app/models/score_details.dart';

class ScoreListController extends GetxController {
  final products = <ScoreDetails>[].obs;
  RxString productId = ''.obs;

  void loadScoreDetailsFromSomeWhere() {
    products.add(
      ScoreDetails(
        name: 'Score added on: ${DateTime.now().toString()}',
        score: 10,
        id: DateTime.now().millisecondsSinceEpoch.toString(),
      ),
    );
  }

  @override
  void onReady() {
    super.onReady();
    loadScoreDetailsFromSomeWhere();
  }

  @override
  void onClose() {
    Get.printInfo(info: 'Products: onClose');
    super.onClose();
  }
}
