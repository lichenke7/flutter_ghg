import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:ghg/app/routes/app_pages.dart';

import '../controllers/score_list_controller.dart';

class ScoreListView extends GetView<ScoreListController> {
  ScoreListView({Key? key}) : super(key: key);
  // final ScoreListController controller = Get.put(ScoreListController());
  // AnnotatedRegion 更改状态栏字体颜色
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      child: Scaffold(
        floatingActionButton: FloatingActionButton.extended(
          onPressed: controller.loadScoreDetailsFromSomeWhere,
          label: const Icon(Icons.add),
        ),
        body: Column(
          children: [
            SizedBox(height: Get.statusBarHeight),
            const Hero(
              tag: 'heroLogo',
              child: FlutterLogo(),
            ),
            Expanded(
              child: Obx(
                () => RefreshIndicator(
                  onRefresh: () async {
                    controller.products.clear();
                    controller.loadScoreDetailsFromSomeWhere();
                  },
                  child: ListView.builder(
                    itemCount: controller.products.length,
                    itemBuilder: (context, index) {
                      final item = controller.products[index];
                      return ListTile(
                        onTap: () {
                          // Navigator.of(Get.context!).push(
                          //   MaterialPageRoute(
                          //     builder: (BuildContext context) {
                          //       return ScoreDetailsView();
                          //     },
                          //     // settings: const RouteSettings(name: Routes.DEBUG_TOOLS),
                          //   ),
                          // );
                          controller.productId.value = item.id;
                          Get.toNamed(
                            Routes.SCORE_DETAILS,
                            // parameters: {'scoreId': item.id},
                          );
                        },
                        title: Text(item.name),
                        subtitle: Text(item.id),
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      value: SystemUiOverlayStyle.dark,
    );
  }
}
