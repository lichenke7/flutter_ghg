import 'package:get/get.dart';
import 'package:ghg/app/base/base_binding.dart';

import '../controllers/score_list_controller.dart';

class ScoreListBinding extends BaseBinding {
  @override
  List<Bind> dependencies() {
    return [
      Bind.lazyPut<ScoreListController>(() => ScoreListController()),
    ];
  }
}
