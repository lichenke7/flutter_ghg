import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:ghg/utils/color_values.dart';
import 'package:ghg/utils/fontweight_ex.dart';

import '../controllers/splash_service.dart';

class SplashView extends GetView<SplashService> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: GColors.primarySwatch,
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Obx(
              () => Text(
                controller.welcomeStr[controller.activeStr.value],
                style: TextStyle(
                  fontSize: 25.sp,
                  color: GColors.white,
                  fontWeight: FontWeightEx.bold,
                ),
              ),
            ),
            SizedBox(height: 20.sp),
            const CircularProgressIndicator(
              color: GColors.white,
            ),
          ],
        ),
      ),
    );
  }
}
