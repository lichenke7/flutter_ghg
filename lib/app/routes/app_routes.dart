// ignore_for_file: non_constant_identifier_names

part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const LOGIN = _Paths.LOGIN;
  static const REGISTER = _Paths.REGISTER;
  static const INVITE_CODE = _Paths.INVITE_CODE;

  static const SCORE_LIST = _Paths.HOME + _Paths.SCORE_LIST;

  static const DASHBOARD = _Paths.HOME + _Paths.DASHBOARD;

  static const PROFILE = _Paths.HOME + _Paths.PROFILE;

  static const SETTINGS = PROFILE + _Paths.SETTINGS;

  static const DEBUG_TOOLS = SETTINGS + _Paths.DEBUG_TOOLS;

  static String LOGIN_THEN(String afterSuccessfulLogin) =>
      '$LOGIN?then=${Uri.encodeQueryComponent(afterSuccessfulLogin)}';
  // static String SCORE_DETAILS(String scoreId) => '$SCORE_LIST/$scoreId';

  static const SCORE_DETAILS = SCORE_LIST + _Paths.SCORE_DETAILS;
}

abstract class _Paths {
  static const HOME = '/home';
  static const SCORE_LIST = '/score_list';
  static const PROFILE = '/profile';
  static const SETTINGS = '/settings';
  static const SCORE_DETAILS = '/details';
  static const LOGIN = '/login';
  static const REGISTER = '/register';
  static const INVITE_CODE = '/invite_code';
  static const DASHBOARD = '/dashboard';
  static const DEBUG_TOOLS = '/debug_tools';
}
