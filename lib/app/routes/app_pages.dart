import 'package:get/get.dart';
import 'package:ghg/app/modules/dashboard/bindings/dashboard_binding.dart';
import 'package:ghg/app/modules/debug/views/debug_tools_page.dart';
import 'package:ghg/app/modules/login/bindings/register_binding.dart';
import 'package:ghg/app/modules/login/invite/bindings/invite_code_binding.dart';
import 'package:ghg/app/modules/login/invite/views/invite_code_view.dart';
import 'package:ghg/app/modules/login/views/register_view.dart';
import 'package:ghg/app/modules/profile/bindings/profile_binding.dart';
import 'package:ghg/app/modules/profile/views/profile_view.dart';
import 'package:ghg/app/modules/root/bindings/root_binding.dart';
import 'package:ghg/app/modules/root/views/root_view.dart';
import 'package:ghg/app/modules/score_details/views/score_details_view.dart';
import 'package:ghg/app/modules/score_list/bindings/score_list_binding.dart';
import 'package:ghg/app/modules/score_list/views/score_list_view.dart';

import '../middleware/auth_middleware.dart';
import '../modules/dashboard/views/dashboard_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';
import '../modules/settings/bindings/settings_binding.dart';
import '../modules/settings/views/settings_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const initial = Routes.HOME;

  static final routes = [
    GetPage(
      name: '/',
      page: () => RootView(),
      bindings: [RootBinding()],
      participatesInRootNavigator: true,
      preventDuplicates: true,
      middlewares: [
        //only enter this route when authed
        EnsureAuthMiddleware(),
      ],
      children: [
        GetPage(
          middlewares: [
            //only enter this route when not authed
            EnsureNotAuthedMiddleware(),
          ],
          name: _Paths.LOGIN,
          page: () => LoginView(),
          bindings: [LoginBinding()],
        ),
        GetPage(
          name: _Paths.REGISTER,
          page: () => RegisterView(),
          bindings: [RegisterBinding()],
        ),
        GetPage(
          name: _Paths.INVITE_CODE,
          page: () => InviteCodeView(),
          bindings: [InviteCodeBinding()],
        ),
        GetPage(
          preventDuplicates: true,
          name: _Paths.HOME,
          page: () => const HomeView(),
          bindings: [HomeBinding()],
          title: null,
          middlewares: [
            //only enter this route when authed
            EnsureAuthMiddleware(),
          ],
          children: [
            GetPage(
              title: 'Dashboard',
              name: _Paths.DASHBOARD,
              transition: Transition.fadeIn,
              page: () => DashboardView(),
              bindings: [DashboardBinding()],
            ),
            GetPage(
              name: _Paths.SCORE_LIST,
              title: 'Score List',
              page: () => ScoreListView(),
              transition: Transition.fadeIn,
              bindings: [ScoreListBinding()],
              children: [
                GetPage(
                  title: 'Score Details',
                  name: _Paths.SCORE_DETAILS,
                  page: () => ScoreDetailsView(),
                  // bindings: [ScoreDetailsBinding()],
                ),
              ],
            ),
            GetPage(
              name: _Paths.PROFILE,
              page: () => ProfileView(),
              title: 'Profile',
              transition: Transition.fadeIn,
              bindings: [ProfileBinding(), SettingsBinding()],
              children: [
                GetPage(
                  title: 'Settings',
                  name: _Paths.SETTINGS,
                  page: () => SettingsView(),
                  // bindings: [],
                  children: [
                    GetPage(
                      name: _Paths.DEBUG_TOOLS,
                      page: () => const DebugToolsPage(),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ],
    ),
  ];
}
