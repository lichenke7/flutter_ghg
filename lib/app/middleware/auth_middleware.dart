import 'package:get/get.dart';
import 'package:ghg/app/routes/app_pages.dart';
import 'package:ghg/app/services/auth_service.dart';
import 'package:ghg/utils/log.dart';

class EnsureAuthMiddleware extends GetMiddleware {
  @override
  Future<RouteDecoder?> redirectDelegate(RouteDecoder route) async {
    // you can do whatever you want here
    // but it's preferable to make this method fast
    // await Future.delayed(Duration(milliseconds: 500));

    if (!AuthService.to.isLoggedInValue) {
      final pageName = route.pageSettings!.name;
      final newRoute = Routes.LOGIN_THEN(pageName);
      gLog('route---name: $pageName');
      if (pageName == '/') {
        return RouteDecoder.fromRoute(newRoute);
      }
      // if (pageName.contains(Routes.INVITE_CODE)) {
      //   return await super.redirectDelegate(route);
      // }
      // return RouteDecoder.fromRoute(newRoute);
      return await super.redirectDelegate(route);
    }
    return await super.redirectDelegate(route);
  }
}

class EnsureNotAuthedMiddleware extends GetMiddleware {
  @override
  Future<RouteDecoder?> redirectDelegate(RouteDecoder route) async {
    if (AuthService.to.isLoggedInValue) {
      //NEVER navigate to auth screen, when user is already authed
      return null;

      //OR redirect user to another screen
      //return RouteDecoder.fromRoute(Routes.PROFILE);
    }
    return await super.redirectDelegate(route);
  }
}
