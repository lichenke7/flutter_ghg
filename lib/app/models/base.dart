import 'dart:convert';

class BaseHttpModel {
  final String? data;
  final String message;
  final int errorCode;
  final bool success;

  BaseHttpModel({
    required this.success,
    required this.message,
    required this.errorCode,
    this.data,
  });

  factory BaseHttpModel.fromRawJson(String str) =>
      BaseHttpModel.fromJson(json.decode(str) as Map<String, dynamic>);

  String toRawJson() => json.encode(toJson());

  factory BaseHttpModel.fromJson(Map<String, dynamic> json) => BaseHttpModel(
        success: json["success"] as bool,
        message: json["message"] as String,
        errorCode: json["errorCode"] as int,
        data: json["data"] as String,
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "message": message,
        "errorCode": errorCode,
        "data": data,
      };
}
