class ScoreDetails {
  final String name;
  final String id;
  final int score;

  ScoreDetails({
    required this.name,
    required this.id,
    required this.score,
  });
}
