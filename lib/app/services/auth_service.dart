import 'package:get/get.dart';
import 'package:ghg/app/routes/app_pages.dart';

class AuthService extends GetxService {
  static AuthService get to => Get.find();

  /// Mocks a login process
  final isLoggedIn = false.obs;
  bool get isLoggedInValue => isLoggedIn.value;

  void login({
    required String name,
    required String pwd,
  }) {
    isLoggedIn.value = true;
  }

  void logout() {
    isLoggedIn.value = false;
    Get.offNamed(Routes.LOGIN);
  }
}
