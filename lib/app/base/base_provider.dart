import 'package:get/get.dart';
import 'package:ghg/app/models/base.dart';

class BaseProvider extends GetConnect {
  // TODO
  final String baseUrl = '';

  @override
  void onInit() {
    super.onInit();
    httpClient.baseUrl = baseUrl;
    httpClient.defaultDecoder =
        (val) => BaseHttpModel.fromJson(val as Map<String, dynamic>);
    // 请求拦截
    httpClient.addRequestModifier<void>((request) {
      request.headers['Authorization'] = '12345678';
      return request;
    });
    // 响应拦截
    httpClient.addResponseModifier((request, response) => response);
  }
}
