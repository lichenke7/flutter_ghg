import 'package:get/get.dart';

abstract class BaseBinding extends Binding {
  @override
  List<Bind> dependencies();
}
