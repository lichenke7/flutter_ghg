import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

abstract class BaseGetView<T extends BaseGetXController> extends GetView {
  const BaseGetView({Key? key}) : super(key: key);

  Widget buildView(BuildContext context);

  Widget build(BuildContext context) {
    return Stack(
      children: [
        buildView(context),
      ],
    );
  }
}
